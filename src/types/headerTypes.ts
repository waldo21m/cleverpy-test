export interface HeaderProps {
	isClosing: boolean;
	mobileOpen: boolean;
	setMobileOpen: (mobileOpen: boolean) => void;
}
